﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public Transform lookUpPoint;
    public Hand hand;

    #endregion

    #region PARAMS

    bool isClicked;

    #endregion

    #region PROPERTIES

    public static InputManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    public void Refresh()
    {
        isClicked = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) /* && GameManager.Instance.currentState == GameState.INGAME && !isClicked */)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue, 1 << 8))
            {
                isClicked = true;

                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
                {
                    lookUpPoint.position = hit.point;
                    Unit unit = hit.collider.transform.parent.GetComponent<Unit>();
                    if (!unit.isSpawnedBalls)
                    {
                        unit.InitLookUp(lookUpPoint);
                        unit.SpawnBalls(0f);
                    }
                }
            }

            hand.transform.position = Input.mousePosition;
            hand.Show();
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
