﻿using System.Collections;
using System.Collections.Generic;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;
using NaughtyAttributes;

public class Test : MonoBehaviour
{
    public Rigidbody rigid;

    [Button]
    public void Force()
    {
        rigid.AddForce(0f, 0f, 5f, ForceMode.VelocityChange);
    }

}
