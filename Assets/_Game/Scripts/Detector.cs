﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Detector : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS

    bool isCollidedUnit;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Refresh();
    }

    void Refresh()
    {
        isCollidedUnit = false;
    }

    public void Explode(float multiply, float duration)
    {
        transform.DOScale(Vector3.one * multiply, duration).OnComplete(() =>
        {
            GetComponent<MeshRenderer>().enabled = false;
            if (!isCollidedUnit)
            {
                if (!MapManager.Instance.currentMap.IsCompleted)
                {
                    // Debug.Log("LOSE!");
                    GameManager.Instance.LoseGame();
                }
                else
                {

                    DataManager.Instance.currentLevel++;
                    DataManager.Instance.SaveData();
                    // Debug.Log("WIN!");
                    if (DataManager.Instance.currentLevel % 3 == 0)
                    {
                        //Finish Level
                        GameManager.Instance.WinGame();
                    }
                    else
                    {
                        //Finish Task
                        StartCoroutine(C_OnFinishTask());
                    }
                }
            }
        });
    }

    IEnumerator C_OnFinishTask()
    {
        GameManager.Instance.PlayConfetti(1);
        yield return new WaitForSeconds(1f);
        GameManager.Instance.PlayGame();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Unit"))
        {
            Unit unit = other.gameObject.GetComponent<Unit>();

            if (!unit.isSpawnedBalls)
            {
                unit.InitLookUp(transform);
                float distance = unit.GetDistanceToLookUp();
                // Debug.Log("Detected Distance: " + distance);
                if (distance <= GameConfiguration.Instance.detectionDistance)
                {
                    unit.SpawnBalls(0.4f);
                    isCollidedUnit = true;
                }

            }
        }
    }


    #endregion

    #region DEBUG
    #endregion

}
