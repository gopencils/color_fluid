﻿using UnityEngine;

public class PoolLocation : MonoBehaviour
{
    public static PoolLocation Instance { get; private set; }


    private void Awake()
    {
        Instance = this;
    }
}
