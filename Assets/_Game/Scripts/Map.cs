﻿using System.Collections;
using System.Collections.Generic;
using PaintIn3D;
using UnityEngine;

public class Map : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    #endregion

    #region PARAMS

    public List<Unit> unitsList = new List<Unit>();

    #endregion

    #region PROPERTIES
    private int filledUnitAmount;

    public int FilledUnitAmount
    {
        get
        {
            filledUnitAmount = 0;
            foreach (Unit unit in unitsList)
            {
                if (unit.isFilled)
                {
                    filledUnitAmount++;
                }
            }
            return filledUnitAmount;
        }
        set
        {
            filledUnitAmount = value;
        }
    }

    private bool isCompleted;

    public bool IsCompleted
    {
        get
        {
            this.isCompleted = (FilledUnitAmount > 0) && (FilledUnitAmount >= unitsList.Count);
            return this.isCompleted;
        }
        set
        {
            this.isCompleted = value;
        }
    }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        FilledUnitAmount = 0;
        IsCompleted = false;
        InitializeUnits();
        // Debug.Log("Refresh Map");
    }

    public void InitializeUnits()
    {
        unitsList.Clear();

        Unit[] units = GameObject.FindObjectsOfType<Unit>();

        for (int i = 0; i < units.Length; i++)
        {
            if (units[i] != null)
            {
                unitsList.Add(units[i]);
            }
        }
    }

    #endregion

    #region DEBUG
    #endregion

}
