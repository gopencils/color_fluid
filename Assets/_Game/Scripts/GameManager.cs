﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, MENU, INGAME, WINGAME, LOSEGAME
}

public class GameManager : MonoBehaviour
{
    #region CONST
    #endregion

    #region EDITOR PARAMS

    public ParticleSystem confettiPSLevel;
    public ParticleSystem confettiPSTask;

    #endregion

    #region PARAMS
    public GameState currentState;
    #endregion

    #region PROPERTIES
    public static GameManager Instance { get; private set; }
    #endregion

    #region EVENTS
    public static event System.Action<GameState> OnStateChanged;
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 60;
    }

    public bool firstTime = false;
    private void Start()
    {
        StartGame();
        MapManager.Instance.SpawnMap();
        firstTime = true;
    }

    public void StartGame()
    {
        ChangeState(GameState.MENU);
    }

    public void PlayGame()
    {
        DetectorPool.Instance.ReturnAll();

        if (firstTime)
        {
            firstTime = false;
        }
        else
        {
            MapManager.Instance.SpawnMap();
        }
        ChangeState(GameState.INGAME);

        InputManager.Instance.Refresh();
    }

    public void RestartGame()
    {
        ChangeState(GameState.INGAME);
        MapManager.Instance.RespawnMap();
        InputManager.Instance.Refresh();
    }

    public void WinGame()
    {
        ChangeState(GameState.WINGAME);

        DetectorPool.Instance.ReturnAll();
        MapManager.Instance.currentMap.gameObject.SetActive(false);

        PlayConfetti(0);

    }

    public void PlayConfetti(int index) //0 : Level   1 : Task
    {
        StartCoroutine(C_PlayConfetti(index));
    }
    IEnumerator C_PlayConfetti(int index)
    {
        ParticleSystem particleSystem = index == 0 ? confettiPSLevel : confettiPSTask;

        particleSystem.gameObject.SetActive(true);
        particleSystem.Stop();
        particleSystem.Play();
        yield return new WaitForSeconds(1f);
        particleSystem.gameObject.SetActive(false);
    }

    public void LoseGame()
    {
        ChangeState(GameState.LOSEGAME);
        DetectorPool.Instance.ReturnAll();

    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChanged != null)
        {
            OnStateChanged(state);
        }
    }

    #endregion
}
