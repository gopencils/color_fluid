﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS
    [Header("SCREENS")]
    public GameObject Menu;
    public GameObject InGame;
    public GameObject WinGame;
    public GameObject LoseGame;

    [Header("INGAME")]
    public Image imgFill;
    public Text txtLevel;
    public Transform iconGemTopRight;
    public Text txtGem;


    [Header("WINGAME")]
    public Transform gemRectTF;
    // public Text txtLevelWG;

    // [Header("LOSEGAME")]
    // public Text txtLevelLG;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    public static UIManager Instance { get; private set; }
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        RegisterEvents();
    }

    private void OnDisable()
    {
        UnregisterEvents();
    }

    private void RegisterEvents()
    {
        GameManager.OnStateChanged += OnGameStateChanged;
    }

    private void UnregisterEvents()
    {
        GameManager.OnStateChanged -= OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState state)
    {
        UpdateLevel();
        UpdatePercentage();


        switch (state)
        {
            case GameState.MENU:
                OnMenu();
                break;
            case GameState.INGAME:
                OnInGame();
                break;
            case GameState.WINGAME:
                OnWinGame();
                break;
            case GameState.LOSEGAME:
                OnLoseGame();
                break;
        }
    }

    public void OnMenu()
    {
        Menu.SetActive(true);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);

    }

    public void OnInGame()
    {
        Menu.SetActive(false);
        InGame.SetActive(true);
        WinGame.SetActive(false);
        LoseGame.SetActive(false);
    }

    public void OnWinGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(true);
        LoseGame.SetActive(false);

        SpawnGemPS(gemRectTF, iconGemTopRight, 1f);
    }

    public void OnLoseGame()
    {
        Menu.SetActive(false);
        WinGame.SetActive(false);
        LoseGame.SetActive(true);
    }

    public void UpdatePercentage()
    {
        float percent = 0f;
        if (DataManager.Instance.currentLevel + 1 > 3)
        {
            percent = (float)((DataManager.Instance.currentLevel + 1) % 3) / 3f;
        }
        else
        {
            percent = (float)(DataManager.Instance.currentLevel + 1) / 3f;
        }
        imgFill.fillAmount = percent;
    }

    public void UpdateLevel()
    {
        int temp = (int)((DataManager.Instance.currentLevel + 1) / 3) + 1;
        txtLevel.text = "LEVEL " + temp;
    }


    public void SpawnGemPS(Transform from, Transform to, float delay)
    {
        StartCoroutine(C_SpawnGemPS(from, to, delay));
    }

    IEnumerator C_SpawnGemPS(Transform from, Transform to, float delay)
    {
        yield return new WaitForSeconds(delay);
        int rand = (int)Random.Range(15f, 20f);
        for (int i = 0; i < rand; i++)
        {
            GemParticle gem = GemParticlePool.Instance.GetFromPool();
            gem.transform.SetParent(from);
            gem.transform.localPosition = Vector3.zero;
            gem.transform.DOLocalMove(Random.insideUnitCircle * 50f, 0.5f).OnComplete(() =>
            {
                gem.transform.DOMove(to.transform.position, 1f).SetEase(Ease.InQuart).OnComplete(() =>
                {
                    UpdateGem();
                    GemParticlePool.Instance.ReturnToPool(gem);
                });
            });
        }
    }

    public void UpdateGem()
    {
        txtGem.DOText("15", 0.5f, true, ScrambleMode.Numerals);
        txtGem.transform.DOScale(Vector3.one * 1.5f, 0.15f).OnComplete(() =>
          {
              txtGem.transform.DOScale(Vector3.one, 0.35f);
          });
    }

    #endregion
}