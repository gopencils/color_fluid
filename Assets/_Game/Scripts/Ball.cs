﻿using System.Collections;
using System.Collections.Generic;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;

public class Ball : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    // public MeshRenderer meshRenderer;
    public Unit unitParent;
    public SplineController splineController;
    public Collider col;
    public bool canSpawnDetector;
    public Transform painter;

    #endregion

    #region PARAMS


    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        painter = transform.GetChild(0);
    }

    private void OnEnable()
    {
        // meshRenderer.enabled = false;
        painter.gameObject.SetActive(false);
        DisableCollider();
    }

    void EnableCollider()
    {
        col.enabled = true;
    }

    void DisableCollider()
    {
        col.enabled = false;
    }

    public void SetRelativePosition(float value)
    {
        splineController.Position = value;
    }

    public void Play()
    {
        splineController.Play();
        painter.gameObject.SetActive(true);

        Invoke("EnableCollider", 0.1f);

    }

    public void Pause()
    {
        splineController.Pause();
    }

    public void Stop()
    {
        splineController.Stop();
        DisableCollider();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            Ball ball = other.GetComponent<Ball>();
            if (ball.unitParent == this.unitParent)
            {
                // OnCollidedBall(other);
                painter.gameObject.SetActive(false);

                unitParent.isFilled = true;
                Pause();

                Vector3 collidePosition = (this.transform.position + other.transform.position) / 2f;
                if (canSpawnDetector)
                {
                    Detector detector = DetectorPool.Instance.GetFromPool();
                    detector.transform.position = collidePosition;
                    detector.Explode(GameConfiguration.Instance.detectionDistance * 2f, 1f);

                    WaterExplosion waterExplosion = WaterExplosionPool.Instance.GetFromPool();
                    waterExplosion.transform.position = collidePosition + Vector3.up;
                    waterExplosion.transform.rotation = Quaternion.identity;
                    waterExplosion.AutoRecycle(1f);

                }

                gameObject.SetActive(false);
            }
        }
    }

    void OnCollidedBall(Collider other)
    {
        StartCoroutine(C_OnCollidedBall(other));
    }

    IEnumerator C_OnCollidedBall(Collider other)
    {
        yield return null;
        painter.gameObject.SetActive(false);

        unitParent.isFilled = true;
        Pause();

        Vector3 collidePosition = (this.transform.position + other.transform.position) / 2f;
        if (canSpawnDetector)
        {
            Detector detector = DetectorPool.Instance.GetFromPool();
            detector.transform.position = collidePosition;
            detector.Explode(GameConfiguration.Instance.detectionDistance * 2f, 1f);

            WaterExplosion waterExplosion = WaterExplosionPool.Instance.GetFromPool();
            waterExplosion.transform.position = collidePosition + Vector3.up;
            waterExplosion.transform.rotation = Quaternion.identity;
            waterExplosion.AutoRecycle(1f);
        }

        gameObject.SetActive(false);
    }

    #endregion

    #region DEBUG
    #endregion

}
