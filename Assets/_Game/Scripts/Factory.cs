﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[ExecuteInEditMode]
public class Factory : MonoBehaviour
{

    public List<GameObject> mapsList = new List<GameObject>();
    public List<Transform> circlesList = new List<Transform>();

    public GameObject planePrefab;

    [Button]
    public void ChangeCircle234567()
    {
        for (int i = 0; i < circlesList.Count; i++)
        {
            Transform curvyShape = circlesList[i].GetChild(2);
            Transform six = curvyShape.GetChild(6);
            six.localPosition = new Vector3(1.5f, six.localPosition.y, 0f);
            Transform seven = curvyShape.GetChild(7);
            seven.localPosition = new Vector3(1.5f, six.localPosition.y, 0f);

            Transform two = curvyShape.GetChild(2);
            two.localPosition = new Vector3(-0.2f, two.localPosition.y, -0.2f);
            Transform three = curvyShape.GetChild(3);
            three.localPosition = new Vector3(-0.2f, three.localPosition.y, -0.2f);

            Transform four = curvyShape.GetChild(4);
            four.localPosition = new Vector3(0.2f, four.localPosition.y, 0.2f);
            Transform five = curvyShape.GetChild(5);
            five.localPosition = new Vector3(0.2f, five.localPosition.y, 0.2f);
        }
    }

    [Button]
    public void AddPlaneAround()
    {
        for (int i = 0; i < mapsList.Count; i++)
        {
            GameObject plane = Instantiate(planePrefab);
            plane.transform.SetParent(mapsList[i].transform);
            plane.transform.localPosition = new Vector3(0f, 0.299f, 0f);
            plane.transform.localRotation = Quaternion.identity;
        }
    }


}
