﻿using System.Collections;
using System.Collections.Generic;
using FluffyUnderware.Curvy;
using UnityEngine;
using NaughtyAttributes;
using FluffyUnderware.Curvy.Generator;
using FluffyUnderware.Curvy.Generator.Modules;
using FluffyUnderware.Curvy.Shapes;

public class Unit : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public CurvySpline curvySpline;
    public Ball ball1;
    public Ball ball2;

    #endregion

    #region PARAMS
    Transform lookUp;
    public bool isSpawnedBalls;
    public bool isFilled;

    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        isSpawnedBalls = false;
        isFilled = false;
        lookUp = null;

        ball1.gameObject.SetActive(false);
        ball2.gameObject.SetActive(false);
    }

    void SetRelativePosition(float value)
    {

        ball1.gameObject.SetActive(true);
        ball2.gameObject.SetActive(true);

        ball1.SetRelativePosition(value);
        ball2.SetRelativePosition(value);

    }

    void PlayBalls()
    {
        ball1.Play();
        ball2.Play();
    }

    void PauseBalls()
    {
        ball1.Pause();
        ball2.Pause();
    }

    void StopBalls()
    {

        ball1.gameObject.SetActive(false);
        ball2.gameObject.SetActive(false);

        ball1.Stop();
        ball2.Stop();
    }

    public void InitLookUp(Transform tf)
    {
        this.lookUp = tf;
    }

    public float GetDistanceToLookUp()
    {
        float distance = 0f;
        var lookupPos = curvySpline.transform.InverseTransformPoint(lookUp.position);
        float nearestTF = curvySpline.GetNearestPointTF(lookupPos);

        Vector3 posOnSpline = curvySpline.transform.TransformPoint(curvySpline.Interpolate(nearestTF));
        distance = (lookUp.position - posOnSpline).magnitude;
        Debug.DrawLine(lookUp.position, posOnSpline, Color.red, 100f);
        Debug.DrawLine(lookUp.position, new Vector3(lookUp.position.x, 0f, lookUp.position.z + 1.3f), Color.blue, 100f);

        return distance;

    }

    float GetPositionOnSpline()
    {
        float pos;
        if (curvySpline && curvySpline.IsInitialized && lookUp && curvySpline.Dirty == false)
        {
            // convert Lookup position to Spline's local space
            var lookupPos = curvySpline.transform.InverseTransformPoint(lookUp.position);
            // get the nearest point's TF on spline
            float nearestTF = curvySpline.GetNearestPointTF(lookupPos);

            float distance = curvySpline.TFToDistance(nearestTF);
            // Debug.Log("Distance: " + distance);
            float position = distance;
            // Debug.Log("position: " + position);
            pos = position;
            // Debug.Log("Position: " + pos);
        }
        else
        {
            pos = 0f;
        }
        return pos;
    }

    public void SpawnBalls(float delay)
    {
        StopBalls();
        SetRelativePosition(GetPositionOnSpline());
        Invoke("PlayBalls", delay);
        isSpawnedBalls = true;

    }

    #endregion

    #region DEBUG
    #endregion

}
