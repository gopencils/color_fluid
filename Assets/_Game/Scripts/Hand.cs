﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Hand : MonoBehaviour
{
    public GameObject hand;
    public Image circleSR;

    Tween scaleTween;
    Tween colorTween;

    private void OnEnable()
    {
        Hide();
    }

    public void Show()
    {

        if (scaleTween != null)
        {
            DOTween.Kill(scaleTween);
        }

        if (colorTween != null)
        {
            DOTween.Kill(colorTween);
        }

        hand.SetActive(true);
        circleSR.gameObject.SetActive(true);

        circleSR.transform.localScale = new Vector3(0f, 0f, 1f);
        circleSR.color = new Color(circleSR.color.r, circleSR.color.g, circleSR.color.b, 1f);


        scaleTween = circleSR.transform.DOScale(Vector3.one * 2f, 0.5f);
        colorTween = circleSR.DOColor(new Color(circleSR.color.r, circleSR.color.g, circleSR.color.b, 0f), 0.5f).OnComplete(() =>
        {
            Hide();
        });
    }

    public void Hide()
    {
        hand.SetActive(false);
        circleSR.gameObject.SetActive(false);
        circleSR.transform.localScale = new Vector3(0f, 0f, 1f);
        circleSR.color = new Color(circleSR.color.r, circleSR.color.g, circleSR.color.b, 1f);
    }
}
