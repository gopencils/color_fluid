﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

public class MapManager : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public List<Map> mapsList;

    #endregion

    #region PARAMS

    public Map currentMap;
    public int lastIndex;

    #endregion

    #region PROPERTIES

    public static MapManager Instance { get; private set; }

    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    private void Awake()
    {
        Instance = this;
    }


    public void SpawnMap()
    {
        StartCoroutine(C_SpawnMap());
    }

    IEnumerator C_SpawnMap()
    {
        Vector3 pos;
        if (currentMap)
        {
            pos = Vector3.zero + Vector3.right * 10f;
        }
        else
        {
            pos = Vector3.zero;
        }

        int temp = DataManager.Instance.currentLevel;

        temp = temp > mapsList.Count - 1 ? (int)Random.Range(0f, mapsList.Count) : temp;

        lastIndex = temp;

        Map map = mapsList[temp];
        yield return new WaitForEndOfFrame();

        Map newMap = Instantiate(map, pos, Quaternion.identity);

        newMap.transform.DOMoveX(0f, 1f);

        if (currentMap)
        {
            currentMap.transform.DOMoveX(-10f, 1f);
            yield return new WaitForSeconds(1f);
            Destroy(currentMap.gameObject);
        }

        currentMap = newMap;

    }

    public void RespawnMap()
    {
        StartCoroutine(C_ReSpawnMap());
    }

    IEnumerator C_ReSpawnMap()
    {
        if (currentMap)
        {
            Destroy(currentMap.gameObject);
            currentMap = null;
        }

        Map map = mapsList[lastIndex];
        yield return new WaitForEndOfFrame();

        currentMap = Instantiate(map, Vector3.zero, Quaternion.identity);
    }

    public void SpawnMap(int index)
    {
        if (currentMap)
        {
            Destroy(currentMap.gameObject);
            currentMap = null;
        }

        Map map = mapsList[index];
        currentMap = Instantiate(map, Vector3.zero, Quaternion.identity);
    }

    #endregion

    #region DEBUG
    #endregion

}
