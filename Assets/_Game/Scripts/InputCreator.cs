﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class InputCreator : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    // public Transform lookUpPoint;
    public GameObject detectorPrefab;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES


    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    #endregion

    #region DEBUG
    
    void SpawnDectetor()
    {
        GameObject detector = Instantiate(detectorPrefab);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.S))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue))
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
                {
                    Detector detector = DetectorPool.Instance.GetFromPool();
                    detector.transform.position = hit.point;
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, float.MaxValue))
            {
                if (hit.collider.gameObject.CompareTag("Detector"))
                {
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }

    #endregion

}
