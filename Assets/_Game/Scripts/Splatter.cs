﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splatter : MonoBehaviour
{

    #region CONST
    #endregion

    #region EDITOR PARAMS

    public SpriteRenderer spriteRenderer;
    public List<Sprite> spritesList;

    #endregion

    #region PARAMS
    #endregion

    #region PROPERTIES
    #endregion

    #region EVENTS
    #endregion

    #region METHODS

    public void SetVisual()
    {
        spriteRenderer.sprite = spritesList[(int)Random.Range(0f, spritesList.Count)];
    }

    #endregion

    #region DEBUG
    #endregion

}
