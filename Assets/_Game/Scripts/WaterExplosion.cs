﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterExplosion : MonoBehaviour
{
    public void AutoRecycle(float delay)
    {
        StartCoroutine(C_AutoRecycle(delay));
    }

    IEnumerator C_AutoRecycle(float delay)
    {
        yield return new WaitForSeconds(delay);

        WaterExplosionPool.Instance.ReturnToPool(this);

    }

}
